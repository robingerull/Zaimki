export default [
    'pronoun_n',
    'pronoun_d',
    'pronoun_a',
    'pronoun_g',
    'article_n',
    'article_g',
    'article_d',
    'article_a',
    'demonstrative_n',
    'demonstrative_g',
    'demonstrative_d',
    'demonstrative_a',
];
